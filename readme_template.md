<!-- PROJECT LOGO  should always be somewhat the twitter card size-->
![gitlab repo card](/images/gitlab-repo-card-1280-640.png)


## beautiful_gitlab_readme
<!-- Description should be a story in sentence fragment form describing the project's value proposition to the developer, designer, etc.The general story form is of a short joke. Namely the struggle, the discovery, and the surprise. I found I could not do AAA until I shot Buddha and this Project helps you Shoot Buddha.-->

A template for beautiful gitlab readmes

[**Explore the docs**](https://gitlab.com/fred.grott/beautiful_gitlab_readme)


[View Demo](https://gitlab.com/fred.grott/beautiful_gitlab_readme)


[Report A bug](https://gitlab.com/fred.grott/beautiful_gitlab_readme/-/issues)



[Request a feature](https://gitlab.com/fred.grott/beautiful_gitlab_readme/-/issues)


<!-- PROJECT SHIELDS gitlab has not changed how they integrate with shields.io yet doing to auth tokens but we still can
     read the information  we need without an auth token as of Jan 2021
     replace `https://gitlab.com/api/v4/projects/23873593` and use onw gitlab project ID .-->
![License](https://img.shields.io/badge/dynamic/json.svg?label=License&url=https://gitlab.com/api/v4/projects/23873593?license=true&query=license.name&colorB=yellow)
![Created at](https://img.shields.io/badge/dynamic/json.svg?label=Created%20at&url=https://gitlab.com/api/v4/projects/23873593&query=created_at&colorB=informational)
![Last activity](https://img.shields.io/badge/dynamic/json.svg?label=Last%20activity&url=https://gitlab.com/api/v4/projects/23873593&query=last_activity_at&colorB=informational)
[![Contributors](https://badgen.net/gitlab/contributors/fred.grott/beautiful_gitlab_readme)](https://gitlab.com/fred.grott/beautiful_gitlab_readme/-/graphs/master)
[![Starrers](https://badgen.net/gitlab/stars/fred.grott/beautiful_gitlab_readme)](https://gitlab.com/fred.grott/beautiful_gitlab_readme/-/starrers)
[![Forks](https://badgen.net/gitlab/forks/fred.grott/beautiful_gitlab_readme)](https://gitlab.com/fred.grott/beautiful_gitlab_readme/-/forks)
[![Open Issues](https://badgen.net/gitlab/open-issues/fred.grott/beautiful_gitlab_readme)](https://gitlab.com/fred.grott/beautiful_gitlab_readme/-/issues)

<!-- TABLE OF CONTENTS gitlab does auto via their GFM one needs to add a toc tag onits own line-->


[[_TOC_]]

<!-- ABOUT THE PROJECT -->
## About The Project

<!-- Always wrap to center as you may have more than one screenshot to show.The length of table-of-contents determines whether the screenshot gets placed here or after the project shields as we want at least parth of the screenshots above the fold in the dekstop display of the webpage generated from the markdown.-->
![screenshot](images/screenshot.png)


Why create a great README Template? Saves time :joy: on creating it from scratch and by organizing it this way you repect your audience's time. And, you make it easier for your dev and designer audience to get the gist of what your project is about and the problem it solves.

Why use Emoji's? According to cognitive science and applied cognitive pyschology pictures that have word meanings help word and text comprehension and memory. Using Emoji's saves image creation time while getting a picture in your audeince's heads.

Style-wise, you **DO NOT WANT** the way legal contracts read! You want, since your project targets beginning devs and or designers; actually to have it be easy to read like the _HEAD FIRST BOOK SERIES_ OReilly and Kathy Sierra are famous for. Put in the writing time and tell a story and narrative along with using Emoji's, picutre words, and phrases painting pictures in your audience's minds.


Here's a blank template to get started:
**To avoid retyping too much info. Do a search and replace with your text editor for the following:**
`github_username`, `repo`, `twitter_handle`,
### Built With

* [Atom Editor](https://atom.io)
* [Shield badges generator](https://shields.io)
* [Markdown](http://daringfireball.net/projects/markdown/)

<!-- GETTING STARTED -->
## Getting Started

To get a local copy up and running follow these simple steps.

### Prerequisites

Most front-end application code requires usage with the git tool. To install git:

* [How To Install Git](https://git-scm.com/)

For Flutter Mobile Applications you need to install at least one mobile SDK:

* [How To Install Android SDK](https://anddroid.dev)
* [How To Install iOS SDK on MACOSX install XCope to get it](https://apps.apple.com/us/app/xcode/id497799835?mt=12)

Than install the Flutter SDK

* [How To Insall  the Flutter SDK](https://flutter.dev/docs/get-started/install.html)

Than install an IDE

* [How To install the Android IDE](https://developer.android.com/studio)
* [How To install the MS VSCode IDE](https://code.visualstudio.com/)


### Installation
1.  Clone the repo via _sh_

        git clone https://gitlab.com/fred.grott/beautiful_gitlab_readme.git


2.  Open the project in your Android Studio or MS VSCode IDE


<!-- USAGE EXAMPLES -->
## Usage

Use this space to show useful examples. Screenshots, code examples and demos work well in this space. You may also link to more code example resources.

_For more examples, please refer to the [Documentation](https://example.com)_



<!-- ROADMAP -->
## Roadmap

See the [open issues](https://gitlab.com/fred.grott/beautiful_gitlab_readme/-/issues) for a list of **proposed features (and known issues)**.



<!-- CONTRIBUTING -->
## Contributing

Contributions are what make the open source community such an interesting place to be learn, inspire, and create. Any contributions you make are **greatly appreciated**.

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

<!-- CONTRIBUTORS-->
### Contributors

You always should list your project contributors
* [Feature contributed, contribuor_name](their_git_repo_profile_url)[twitter_handle](their_twitter_profile_url)
* []()
* []()


<!-- LICENSE -->
## License

Distributed under the BSD License. See [LICENSE](https://gitlab.com/fred.grott/beautiful_gitlab_readme/-/blob/master/LICENSE) for more information.



<!-- CONTACT -->
## Contact
<!-- email addy is always stated in an expanded way as it obfuscates it from picked up by web scrapers and generating spam emails-->
Fred Grott - [@twitter_handle](https://twitter.com/fredgrott) - email: fred DOT grott AT gmail DOT com

Project Link: [https://gitlab.com/fred.grott/beautiful_gitlab_readme](https://gitlab.com/fred.grott/beautiful_gitlab_readme)



<!-- ACKNOWLEDGEMENTS -->
## Acknowledgements

* [Othneil Drew's Best-README-Template](https://github.com/othneildrew/Best-README-Template)
* [Matias Singers' Awesome List of READMEs and README writing resources](https://github.com/matiassingers/awesome-readme)
* [asodi gitlab badges-shields](https://gitlab.com/asdoi/gitlab-badges)
* [Gitlab's Markdown including its GFM](https://about.gitlab.com/handbook/markdown-guide/)
* [shields.io](https://shields.io)
* [badgen](https://badgen.net/)
* [Extended Markdown Syntax](https://www.markdownguide.org/extended-syntax/)
* [Emojipedia](https://emojipedia.org/)
* [Markdown Spec](http://daringfireball.net/projects/markdown/)

<!-- RESOURCES-->
<!-- You should put some resources to the computer language, frameworks, etc as it decreases someone opening issues of how to use that computer laguage or framework.-->
## Flutter Community Resources
* [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
* [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)
* [Flutter online documentation](https://flutter.dev/docs)
* [Dart online documentation](https://dart.dev/docs)
* [Ask Flutter Dev questions@Stackoverflow](https://stackoverflow.com/tags/flutter)
* [Ask Flutter Dev questions@Reddit](https://www.reddit.com/r/FlutterDev/)
* [Flutter Community Articles@Medium.com](https://medium.com/flutter-io)
* [Solido Awesome Flutter List Of Resources](https://github.com/Solido/awesome-flutter)
* [Flutter Dev Videos@Youtube](https://www.youtube.com/playlist?list=PLOU2XLYxmsIJ7dsVN4iRuA7BT8XHzGtCr)
* [Ask questions about Flutter@Hashnode](https://hashnode.com/n/flutter)
